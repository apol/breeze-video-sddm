/*
 *   Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
import QtMultimedia 5.8

FocusScope {
    id: sceneBackground

    property var sceneBackgroundType
    property color sceneBackgroundColor
    property url sceneBackgroundUrl

    Loader {
        anchors.fill: parent
        sourceComponent: switch(sceneBackground.sceneBackgroundType) {
            case "color": return colorComponent;
            case "image": return imageComponent;
            case "video": return videoComponent;
        }
    }

    Component {
        id: colorComponent
        Rectangle {
            color: sceneBackground.sceneBackgroundColor
        }
    }

    Component {
        id: imageComponent
        Image {
            fillMode: Image.PreserveAspectCrop
            smooth: true
            source: sceneBackground.sceneBackgroundUrl
        }
    }

    Component {
        id: videoComponent

        VideoOutput {
            fillMode: VideoOutput.PreserveAspectCrop
            source: MediaPlayer {
                id: mediaplayer
                autoPlay: true
                loops: MediaPlayer.Infinite
                source: sceneBackground.sceneBackgroundUrl
            }
        }
    }
}
